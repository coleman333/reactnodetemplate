FROM   node:6.11.4

COPY ./cok
WORKDIR ./cok
RUN npm i && npm run build

EXPOSE 80 433

ENTRYPOINT "npm run"
CMD "start"