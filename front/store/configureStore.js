import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import rootReducer from 'app/reducers';

function configureStore(initialState = {}) {

	let middleware = [thunk];
	try {
		if (__DEV__ && !__SERVER__) {
			middleware.push(logger);
		}
	} catch (ex) {
		// console.error(ex);
	}
	// Installs hooks that always keep react-router and redux
	// store in sync
	const finalCreateStore = applyMiddleware(...middleware)(createStore);

	const store = finalCreateStore(rootReducer, initialState);

	if (module.hot) {
		// Enable Webpack hot module replacement for reducers
		module.hot.accept('reducers', () => {
			const nextReducer = rootReducer;
			store.replaceReducer(nextReducer);
		});
	}
	return store;
}

export default configureStore;