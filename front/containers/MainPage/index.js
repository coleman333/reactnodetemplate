import "app/assets/styles/main.scss";
// import './styles.scss';
import React, {Component} from "react";
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from "react-redux";
import {browserHistory} from 'react-router';
// import  from 'react-router';


class MainPage extends Component {
	static propTypes = {};

	state = {};

	constructor(props) {
		super(props);
	}

	authFB(){
		// this.props.authFacebook
	}

	render() {

		const {children,} = this.props;

		return <div className="">
			MAIN page
			<button onClick={this.authFB.bind(this)}>facebook</button>
			<a href="https://localhost:4433/api/auth/facebook">ClickMe</a>
			<a href="https://localhost:4433/api/auth/instagram">ClickMeInstagram</a>
			{children}
		</div>
	}

}

const mapStateToProps = (state) => {
	return {}
};

const mapDispatchToProps = (dispatch) => {
	return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
