const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const logActionSchema = new Schema({
    name:{type: String,required: false},
    email: {type: String, required: false, unique: true},
    phone:{type:String,required:false},
    staffName:{type:String,required:true},
    amount:{type:String,required:true},
    action:{type:String,required:true}
});
module.exports = mongoose.model('LogAction',logActionSchema);