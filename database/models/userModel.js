const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;

const userSchema = new Schema({
	name: {type: String, required: false},
	email: {type: String, required: false, unique: true},
	phone: {type: String, required: false},
	role: {type: String, required: false},
	pushToken: {type: String},
	facebookId: {type: String},
	instagramId: {type: String},
	instagramAccessToken: {type: String},

	// firstName: {type: String, required: false},
	// lastName: {type: String, required: true},
	// email: {type: String, required: true, unique: true},
	// password: {type: String, required: true},
	// avatar: {type: String, required: false}
});

// userSchema.pre('save', function (next) {
//     const user = this;
//
//     // only hash the password if it has been modified (or is new)
//     if (!user.isModified('password')) return next();
//
//     // generate a salt
//     bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
//         if (err) return next(err);
//
//         // hash the password along with our new salt
//         bcrypt.hash(user.password, salt, (err, hash) => {
//             if (err) return next(err);
//
//             // override the cleartext password with the hashed one
//             user.password = hash;
//             next();
//         });
//     });
// });

// userSchema.methods.comparePassword = function (candidatePassword, cb) {
//     bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
//         if (err) return cb(err);
//         cb(null, isMatch);
//     });
// };

module.exports = mongoose.model('User', userSchema);

