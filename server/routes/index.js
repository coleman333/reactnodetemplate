// App is a function that requires store data and url to initialize and return the React-rendered html string - server-side rendering
const App = require('../../public/assets/front.server.js');
const users = require('./users');
const auth = require('./auth');

module.exports = (app) => {
	// app.use((req,res,next)=>{
	//     console.log('Request.',req);
	//     next();
	// });
	app.use('/api/users', users);
	app.use('/api/auth', auth);

	app.get('*', (req, res, next) => {
		App(req, res);
	});

// error handler
// development error handler
// will print stacktrace

	app.use((err, req, res, next) => {
		console.error(err);
		res.status(err.status || 500);
		res.json({
			message: err.message,
			error: app.get('env') === 'development' ? err : {}
		});
	});

};
