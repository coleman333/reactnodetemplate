const express = require('express');
const router = express.Router();
const passport = require('passport');
const userCtrl = require('../controllers/users');
router.get('/facebook', passport.authenticate('facebook'));
router.get('/facebook/callback', async (req, res, next) => {
	await passport.authenticate('facebook', async (error, user) => {
		if (error) {
			return next(error)
		}

		await require('util').promisify(req.login).call(req, user);
// res.end();
		res.redirect(`https://localhost:4433/success`);
	})(req, res, next);
});

router.get('/instagram', passport.authenticate('instagram'));
router.get('/instagram/callback', async (req, res, next) => {
	await passport.authenticate('instagram', async (error, user) => {
		if (error) {
			return next(error)
		}

		await require('util').promisify(req.login).call(req, user);
// res.end();
		res.json({user:req.user});
// 		res.redirect(`https://localhost:4433/success`);
	})(req, res, next);
});
router.get('/instagram/getUser',userCtrl.get)
module.exports = router;