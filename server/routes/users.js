const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/users');


// router.get('/', userCtrl.fetch);
// router.post('/registration', userCtrl.upload.single('avatar'), userCtrl.registration);
// router.post('/login', userCtrl.login);
// router.get('/logout', userCtrl.logout);
// router.post('/isEmail', userCtrl.isEmail);
// console.log("this is server 1234")
//
router.post('/push-token', userCtrl.createPushToken);
router.post('/login', userCtrl.logVisits , userCtrl.login);
router.put('/registration', userCtrl.registration);
router.put('/check-name', userCtrl.checkName);
router.get('/product/:product',userCtrl.getProduct);
router.put('/create-product',userCtrl.createProduct);
// router.put('/auth-linked-in',userCtrl.authLinkedIn);

module.exports = router;


