const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const path = require('path');
const helmet = require('helmet');
const config = require('./index.js');
const logger = require('./logger');
const cluster = require('cluster');
const webpack = require('webpack');
const webpackConfig = require('../../webpack.config.js');
const mongoose = require('mongoose');
const {database:{username, password, host, port, dbname}} = require('../../database/config');
mongoose.connect(`mongodb://${host}:${port}/${dbname}`, {
    promiseLibrary: global.Promise
});

const cookieParser = require('cookie-parser');
const passport = require('passport');
const passportConfig = require('./passport/config');
passportConfig(passport);
const MongoStore = require('connect-mongo')(session);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
});

const nodeEnv = process.env.NODE_ENV;

const app = express();

const browserWebpackConfig = Array.isArray(webpackConfig) ? webpackConfig.find(conf=>conf.name === 'browser') : webpackConfig;
if (browserWebpackConfig) {
    const compiler = webpack(browserWebpackConfig);

    const isDev = process.env.NODE_ENV === 'development';

    if (isDev) {
        app.use(require('webpack-dev-middleware')(compiler, {
            noInfo: true,
            publicPath: browserWebpackConfig.output.publicPath
        }));

        app.use(require('webpack-hot-middleware')(compiler));
    }
}

// TODO: set 443 port for production
app.set('port', (process.env.PORT || 80));

app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
app.use(helmet.xssFilter());
app.use(helmet.hsts());
app.use(helmet.ieNoOpen());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true})); // for parsing application/x-www-form-urlencoded
app.use(express.static(path.join(__dirname, '../../public')));

// The trust proxy setting is implemented using the proxy-addr package. For more information, see its documentation.
// loopback - 127.0.0.1/8, ::1/128
app.set('trust proxy', 'loopback');

// TODO: set session store
// Create a session middleware with the given options
// Note session data is not saved in the cookie itself, just the session ID. Session data is stored server-side.
let sess = {
    resave: true,
    saveUninitialized: false,
    secret: config.sessionSecret,
    proxy: true, // The "X-Forwarded-Proto" header will be used.
    name: 'sessionId',
    // Add HTTPOnly, Secure attributes on Session Cookie
    // If secure is set, and you access your site over HTTP, the cookie will not be set
    cookie: {
        httpOnly: true,
        secure: false
    },
    store: new MongoStore({mongooseConnection: db})
};

if (nodeEnv === 'production') {
    sess.cookie.secure = true; // Serve secure cookies
}
app.use(session(sess));

// TODO: install and configure passport package for authentication

app.use(passport.initialize());
app.use(passport.session());

// Bootstrap routes
require('../routes')(app);

logger.debug(`Worker ${cluster.worker.id} has been started!`);

module.exports = app;
        