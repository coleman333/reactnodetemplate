const LocalStrategy = require('passport-local').Strategy;
const userModel = require('../../../database/models/userModel');
const FacebookStrategy = require('passport-facebook').Strategy;
const InstagramStrategy = require('passport-instagram').Strategy;

module.exports = (passport) => {
	passport.use(new LocalStrategy(
		{usernameField: 'email'},
		function (email, password, done) {
			userModel.findOne({email: email}, (err, user) => {
				if (err) {
					return done(err);
				}
				if (!user) {
					return done(null, false, {message: 'Incorrect email.'});
				}
				user.comparePassword(password, (err, isMatch) => {
					if (err) {
						return done(err);
					}
					if (!isMatch) {
						return done(null, false, {message: 'Incorrect password.'});
					}
					return done(null, user);
				});
			});
		}
	));

	passport.use(new FacebookStrategy({
			clientID: "254728085376710",
			clientSecret: "08bb74abaac0cdd16b11c51d3dce4b1e",
			callbackURL: "https://localhost:4433/api/auth/facebook/callback",
			authType: 'https',
			cookie: true,
			profileFields: ['id', 'displayName', 'email'],
			scope: ['public_profile', 'email']
		},
		async (accessToken, refreshToken, profile, cb) => {
			try {
				let user = await userModel.findOne({facebookId: profile.id}).exec();
				if (!user) {
					user = await userModel.create({
						name: profile.displayName,
						facebookId: profile.id,
						email: profile._json.email,
					});
				}

				cb(null, user);
			} catch (ex) {
				cb(ex);
			}
		}
	));
	passport.use(new InstagramStrategy({
			clientID: 'f618d75e427548d9ac1d1e488ae7c55f',
			clientSecret: '6635229921314e758aa9fd143783f165',
			callbackURL: "https://localhost:4433/api/auth/instagram/callback",
			authType: 'https',
			cookie: true,
			profileFields: ['id', 'displayName', 'email'],
			scope: ['public_content', 'basic']
		},
		async function (accessToken, refreshToken, profile, cb) {
			try {
				console.log(`\n\n=>`, profile);
				let user = await userModel.findOne({instagramId: profile.id}).exec();
				if (!user) {
					user = await userModel.create({
						name: profile.displayName,
						instagramId: profile.id,
						email: profile._json.email,
						instagramAccessToken: accessToken,
					});
				}

				cb(null, user);
			} catch (ex) {
				cb(ex)
			}
		}
	));

	passport.serializeUser((user, done) => {
		done(null, user._id);
	});

	passport.deserializeUser((id, done) => {
		userModel.findById(id, '-password', (err, user) => {
			done(err, user); //req.user._id
		});
	});
};
